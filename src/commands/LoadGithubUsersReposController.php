<?php

namespace app\commands;

use app\models\GithubUser;
use Exception;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Json;
use linslin\yii2\curl;

/**
 * Получает и сохраняет репозитории юзеров github.
 */
class LoadGithubUsersReposController extends Controller
{
    /**
     * @return int
     * @throws Exception
     */
    public function actionIndex(): int
    {
        $users = GithubUser::find()->select('name')->column();
        $usersRepos = [];
        foreach ($users as $user) {
            $usersRepos[] = static::getUserRepos($user);
        }
        $usersRepos = array_merge(...$usersRepos);

        usort($usersRepos, function ($a, $b) {
            return $b['updated'] <=> $a['updated'];
        });

        $usersRepos = array_slice($usersRepos, 0, 10);

        Yii::$app->cache->set('usersRepos', $usersRepos);
        Yii::$app->cache->set('usersReposDate', time());

        return ExitCode::OK;
    }

    /**
     * @param string $user
     * @return array
     * @throws Exception
     */
    public static function getUserRepos(string $user): array
    {
        $curl = new curl\Curl();

        $data = $curl->setOptions([
            CURLOPT_USERAGENT => 'skybleak',
            CURLOPT_RETURNTRANSFER => true
        ])->get("https://api.github.com/users/$user/repos?sort=updated&per_page=10");

        if ($curl->responseCode != 200) {
            return [];
        }

        $repos = Json::decode($data);

        $resultRepos = [];
        foreach ($repos as $repo) {
            $resultRepos[] = [
                'user' => $user,
                'full_name' => $repo['full_name'],
                'url' => $repo['html_url'],
                'updated' => $repo['updated_at'],
            ];
        }

        return $resultRepos;
    }
}
