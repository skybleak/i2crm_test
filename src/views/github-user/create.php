<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\githubUser */

$this->title = 'Создать Github Пользователя';
$this->params['breadcrumbs'][] = ['label' => 'Github Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="github-user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
