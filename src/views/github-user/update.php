<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\githubUser */

$this->title = 'Обновить Github Пользователя: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Github Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="github-user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
