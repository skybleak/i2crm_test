<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;

$this->beginContent('@app/views/layouts/main.php');
?>
<?= $content ?>
<?= Html::a('Репозитории', '/') ?>
<?php $this->endContent(); ?>