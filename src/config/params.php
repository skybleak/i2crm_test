<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'githubUsers' => [
        'samdark',
        'qiangxue',
        'cebe',
        'SilverFire',
        'klimov-paul',
        'softark',
        'cuileon',
        'creocoder',
        'Ragazzo',
        'bizley',
    ],
];
